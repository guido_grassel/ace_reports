#! /bin/bash

# Script fetches everything needed via SSH to Windows Preview
# run on cygwin

RSYNC="rsync --progress"

# Linux build machine
HOST=g00323864@192.168.1.195

# switch btw baseline and partial update tree
#BASE="~/sources/partial_update_march01"
BASE="~/sources/baseline-march01"

# ETS test case name without file ending, without _debug.
TESTCASE=covid19

echo "Fetching: =========================="
echo "from tree: $BASE"
echo "test case: $TESTCASE"
echo "Fetching: =========================="
echo ""


# Windows EXE and LIBS
$RSYNC $HOST:$BASE/out/sdk/mingw_x86_64/common/common/* .

# scripts with current builds these are in C buffers, no longer need to fetch JS files
#$RSYNC $HOST:$BASE/foundation/ace/ace_engine/frameworks/bridge/declarative_frontend/state_mgmt/dist/stateMgmt.js         scripts/
#$RSYNC $HOST:$BASE/foundation/ace/ace_engine/frameworks/bridge/declarative_frontend/engine/jsEnumStyle.js                scripts/
#$RSYNC $HOST:$BASE/foundation/ace/ace_engine/frameworks/bridge/declarative_frontend/engine/quickjs/jsMockSystemPlugin.js scripts/

# one time run to make a template project using HQ transpiler
# get base project from HQ transpiler
#$RSYNC $HOST:$BASE/developtools/ace-ets2bundle/compiler/sample/build/*  ../preview/js/default_2.0

# compiled JS file to fetch
$RSYNC $HOST:$BASE/ace-compiler-nt/out/"$TESTCASE"_debug.js  ../preview/js/default_2.0/pages/index.js
