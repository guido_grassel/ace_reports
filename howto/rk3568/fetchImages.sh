#! /bin/bash
mkdir -p ./images

# fetch ready compiled images via SSH to Windows, for flashing with RXDevTool.exe
rsync --progress g00323864@192.168.1.195:~/sources2/baseline-march005-rock/out/rk3568/packages/phone/images/* images/

# fetch compiled hdc.exe 
#rsync --progress g00323864@192.168.1.195:~/sources/baseline-march01/out/sdk/ohos-sdk/windows/toolchains/hdc_std.exe .