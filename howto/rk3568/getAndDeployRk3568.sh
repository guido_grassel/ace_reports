#! /bin/bash

# fetches a test case (a JS file) via SSH, pushes it to device, starts the app
# 
# Installs content\entry-debug-signed.hap as a template into which the index.js is written.
# Adjust paths on Windows computer as needed
# note that hdc.exe requires all path on Windows to be with back slash, and on device with froward slash.
# the method is not optimized but it works!

HDC=/cygdrive/d/HiHope-DAYU200/hdc_std.exe
HAP="content\entry-debug-signed.hap"
CONFIG="/data/app/el1/bundle/public/com.huawei.plrdtest.myapplication/com.huawei.plrdtest.myapplication/config.json"
INDEXJS="/data/app/el1/bundle/public/com.huawei.plrdtest.myapplication/com.huawei.plrdtest.myapplication/assets/js/MainAbility/pages/index.js"

RSYNC="rsync --progress"
HOST=g00323864@192.168.1.195
#BASE="~/sources/partial_update_march01"
BASE="~/sources/baseline-march01"
# jst the name, no ending .js, no ending _debug
TESTCASE=covid19

echo "Fetching: =========================="
echo "from tree: $BASE"
echo "test case: $TESTCASE"
echo "Fetching: =========================="
echo ""

$RSYNC $HOST:$BASE/ace-compiler-nt/out/"$TESTCASE"_debug.js  ./index.js


echo "Deploy: =========================="
echo "Install $HAP"
$HDC uninstall com.huawei.plrdtest.myapplication
$HDC install $HAP

echo "Patch to use QJS $CONFIG"
$HDC file send content/config.json $CONFIG

ECHO "Deploy index.js to $INDEXJS"
$HDC file send index.js $INDEXJS


# deploy Covid19 app flags files, ~180 files, takes time
$HDC shell mkdir -p /data/app/el1/bundle/public/com.huawei.plrdtest.myapplication/com.huawei.plrdtest.myapplication/assets/js/MainAbility/flags
cd  /cygdrive/d/Workspace/preview/js/default_2.0/flags
for f in  *.png ; do $HDC file send "$f" /data/app/el1/bundle/public/com.huawei.plrdtest.myapplication/com.huawei.plrdtest.myapplication/assets/js/MainAbility/flags/"$f" ; done
cd -

ECHO "Start app"
$HDC shell aa start -b com.huawei.plrdtest.myapplication -a com.huawei.plrdtest.myapplication.MainAbility

echo "Done!"