
# Partial Update Solution Design 

This document discusses design of key solutions for partial update.
The referenced document gives info about current development branches 
and test status: https://gitee.com/guido_grassel/ace_reports/blob/master/partialUpdateTest.md


# Scenarios

 
## Partial Update without making structural changes to the Component and Element tress

 
### eDSL sources


An example of the most basic functionality, e.g. no `ForEach` no `If` used

```Typescript

@Entry
@Component
struct PartialRerender1 {
    @State s1 : number = 0;
    @State s2 : number = 0;
    @State rowColor : string = this.getRandomColor();

    getRandomColor() : string {
        return "#ff" + this.rndHexStr(255) + this.rndHexStr(255) + this.rndHexStr(255);
    }

    rndHexStr(max : number) : string {
        return Math.floor(Math.random() * max).toString(16);
    }

    build() {
        Column(){
            Row() {
                // dync text label
                Text(`s1=${this.s1}`)
                  .width(200).height(40).fontSize(15)

                // RText dynamic height
                Text("Row1(static):Text2 (dyn height): 40 + s2*10 -> height")
                  .width(100).height(40 + 10*this.s2)
                  .backgroundColor("#b0ffb0")
                  .fontSize(15)
            }
              .backgroundColor("#50ff50")

            // Column dync height
            Row() {
                Text("Row2 (dyn width).Text1(static")
                    .fontSize(15)
                Text("Row2 (dyn width).Text2(static)")
                    .fontSize(15)
            }
                .backgroundColor("#ff5050")
                .width(100).height(50)
                .backgroundColor(this.rowColor)

            Button(`Button1(static) s1 += 1`)
                .onClick(() => {
                    console.log("click handler mod s1 start")
                    this.s1+= 1;
                    console.log("click handler mod s1 done")
                })
                  .width(300).height(60)

            Button(`(Button2(static)s2 += 1`)
                .onClick(() => {
                    console.log("click handler mod s2 start")
                    this.s2+= 1;
                    console.log("click handler mod s2 done")
                })
                  .width(300).height(60)

            Button(`Button3(static) Row bgColor`)
                .onClick(() => {
                    console.log("click handler rowColor start")
                    this.rowColor = this.getRandomColor();
                    console.log("click handler rowColor done")
                })
                  .width(300).height(60)
        }
    }
}
```


### Baseline `render()` function


For reference the transpiler output of baseline solution. The render function is executed for first render and for custom component rerender.

```Typescript

  render(): void {
    Column.create();
    Row.create();
    Row.backgroundColor("#50ff50");
    // dync text label

    Text.create(`s1=${this.s1}`);
    Text.width(200);
    Text.height(40);
    Text.fontSize(15);
    Text.pop();
    // RText dynamic height

    Text.create("Row1(static):Text2 (dyn height): 40 + s2*10 -> height");
    Text.width(100);
    Text.height(40 + 10 * this.s2);
    Text.backgroundColor("#b0ffb0");
    Text.fontSize(15);
    Text.pop();
    Row.pop();
    // Column dync height

    Row.create();
    Row.backgroundColor("#ff5050");
    Row.width(100);
    Row.height(50);
    Row.backgroundColor(this.rowColor);
    Text.create("Row2 (dyn width).Text1(static");
    Text.fontSize(15);
    Text.pop();
    Text.create("Row2 (dyn width).Text2(static)");
    Text.fontSize(15);
    Text.pop();
    Row.pop();
    Button.createWithLabel(`Button1(static) s1 += 1`);
    Button.onClick((() => {
      console.log("click handler mod s1 start")
      this.s1 += 1;
      console.log("click handler mod s1 done")
    }));
    Button.width(300);
    Button.height(60);
    Button.pop();
    Button.createWithLabel(`(Button2(static)s2 += 1`);
    Button.onClick((() => {
      console.log("click handler mod s2 start")
      this.s2 += 1;
      console.log("click handler mod s2 done")
    }));
    Button.width(300);
    Button.height(60);
    Button.pop();
    Button.createWithLabel(`Button3(static) Row bgColor`);
    Button.onClick((() => {
      console.log("click handler rowColor start")
      this.rowColor = this.getRandomColor();
      console.log("click handler rowColor done")
    }));
    Button.width(300);
    Button.height(60);
    Button.pop();
    Column.pop();
  }
}
```

###  Partial update `render`  function

 
The eDSL to TS transpiler transpiles the eDSL `build` function to a `render` function. In addition the transpiler generates a separate 'rerender' function.

The `render` function is sub-divided into lambda functions that create one sub-component and execute all attribute functions on the created component. Each lambdas updates one component. Children of a component are created in separate lambda.


The purpose of sub-dividing into small update functions is two-fold
- at initial render, create the dependency maps: state variable -> Component / Element (its unique ID), and ElementID -> update function
- at re-render execute only those update functions whose state variables have changed.


```TypeScript

public render(): void {
    // do an initial render 
    console.debug(`${this.id__()}:${this.constructor.name}: render() start`);


    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Column elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Column.create();
      if (!isInitialRender) {
        Column.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Column " + elmtId + " - done");
    });


    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Row elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Row.create();
      Row.backgroundColor("#50ff50");
      if (!isInitialRender) {
        Row.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Row " + elmtId + " - done");
    });


    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Text.create(`s1=${this.s1}`);
      Text.width(200);
      Text.height(40);
      Text.fontSize(15);
      if (!isInitialRender) {
        Text.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text " + elmtId + " - done");
    });
    Text.pop();

    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Text.create("Row1(static):Text2 (dyn height): 40 + s2*10 -> height");
      Text.width(100);
      Text.height(40 + 10 * this.s2);
      Text.backgroundColor("#b0ffb0");
      Text.fontSize(15);
      if (!isInitialRender) {
        Text.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text " + elmtId + " - done");
    });
    Text.pop();

    Row.pop();

    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Row elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Row.create();
      Row.backgroundColor("#ff5050");
      Row.width(100);
      Row.height(50);
      Row.backgroundColor(this.rowColor);
      if (!isInitialRender) {
        Row.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Row " + elmtId + " - done");
    });



    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Text.create("Row2 (dyn width).Text1(static");
      Text.fontSize(15);
      if (!isInitialRender) {
        Text.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text " + elmtId + " - done");
    });
    Text.pop();


    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Text.create("Row2 (dyn width).Text2(static)");
      Text.fontSize(15);
      if (!isInitialRender) {
        Text.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text " + elmtId + " - done");
    });
    Text.pop();

    Row.pop();


    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Button elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Button.createWithLabel(`Button1(static) s1 += 1`);
      Button.onClick((() => {
        console.log("click handler mod s1 start")
        this.s1 += 1;
        console.log("click handler mod s1 done")
      }));
      Button.width(300);
      Button.height(60);
      if (!isInitialRender) {
        Button.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Button " + elmtId + " - done");
    });
    Button.pop();


    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Button elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Button.createWithLabel(`(Button2(static)s2 += 1`);
      Button.onClick((() => {
        console.log("click handler mod s2 start")
        this.s2 += 1;
        console.log("click handler mod s2 done")
      }));
      Button.width(300);
      Button.height(60);
      if (!isInitialRender) {
        Button.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Button " + elmtId + " - done");
    });
    Button.pop();


    this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Button elmtId " + elmtId + " start  ....");
      ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
      Button.createWithLabel(`Button3(static) Row bgColor`);
      Button.onClick((() => {
        console.log("click handler rowColor start")
        this.rowColor = this.getRandomColor();
        console.log("click handler rowColor done")
      }));
      Button.width(300);
      Button.height(60);
      if (!isInitialRender) {
        Button.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Button " + elmtId + " - done");
    });
    Button.pop();

    Column.pop();

    console.debug(`${this.id__()}:${this.constructor.name}: render() done`);
  }

```

 
The `View.observeComponentCreation()` function to make above code understandable

```TypeScript

  public observeComponentCreation(compilerAssignedUpdateFunc: UpdateFunc): void {
    const elmtId = ViewStackProcessor.AllocateNewElmetIdForNextComponent();
    console.debug(`${this.constructor.name}[${this.id__()}]: First render for elmtId ${elmtId} start ....`);
    compilerAssignedUpdateFunc(elmtId, /* is first rneder */ true);

    this.updateFuncByElmtId.set(elmtId, compilerAssignedUpdateFunc);  // Map
    console.debug(`${this.constructor.name}[${this.id__()}]: First render for elmtId ${elmtId} - DONE.`);
  }
```

 `ViewStackProcessor.AllocateNewElmetIdForNextComponent()` requests a new unique elemtId for `ElementRegister`. Memorizes this elmtId in VSP.
On every 'get' (read) access on an ObservedPropertyAbstract, the state variable queries the `accountGetAccessToElmtId_` of VSP. Stores the elmtId in its list of dependent elmtId's.


### Rerender


Above `render()` function is only used for first render. Another function is for rerender:

```TypeScript
  // rerender function for re-render case
  rerender(): void {
    console.debug(`${this.id__()}:${this.constructor.name}: rerender() start`);
    // if variable value has changed since last (re)render, mark all its
    // dependent Elements as needing partial updating 
    /* @State s1 : number */
    this.__s1.markDependentElementsDirty(this);
    /* @State s2 : number */
    this.__s2.markDependentElementsDirty(this);
    /* @State rowColor : string */
    this.__rowColor.markDependentElementsDirty(this);
    this.updateDirtyElements();
    console.debug(`${this.id__()}:${this.constructor.name}: rerender() done`);
  }
}
```

The processing sequence
1. set of an observed variable. 
2. Set the ObservedPropertyAbstract.notifyHasChanged() notifies subscriving View, sets propertyHasChanged_ = true;
3. on receiving notification in View.propertyHasChanged, call to View.markNeedUpdate implemented as JSView::MarkNeedUpdate()
4. JSView::MarkNeedUpdate() sets a task to exec. jsViewFunction_->ExecuteRerender(); Posting a task and setting JSView.pendingUpdate_ is to avoid posting duplicate tasks caused by multiple state variable mutations in the same even handler code.
5. rerender executes for each state variable ObservedPropertyAbstract.markDependentElementsDirty(): this func does nothing its the variable has not changed (propertyHasChanged_). If changed, for each dependent elmtId it calls View.markElemenDirtyById(elmtId).
6. At the end of rerender function exec. View.dirtDescendantElementIds_ list includes all elmtIds that require updating (for this @Component).
7. View.updateDirtyElements() processing this list. ForEach element id it looks up the update function (View.updateFuncByElmtId_ map) and executes it.
8. For completion of each update func execution it calls to View.finishUpdateFunc implemented as JSView::JsFinishUpdateFunc. JSView::JsFinishUpdateFunc calls VSP::Finish() to obtain the Components. The returned reference pointing to the outmost wrapping Component. The Main Component is deepest inside this (usually) List of Components.
9. The elmtId and the Compoennt are added to JSView.pendingElementUpdates_ std::list<pair<elmtId, AceRef<Component>>>. So, this is the list of pending Component to Element updates. When adding multithreading to the solution this list will be sent from JS to UI theed. View.finishUpdateFunc() also marks the Element dirty (Element::MarkDirty()).  This also means that all processing up to here is triggered by state variable mutation and its processed as fast as possible. 
10. renderFunction lambda given to the Element (see JSView::CreateComponent()) calls JSView::MakeElementUpdatesToCompleteRerender()
11. JSView::ComponentToElementLocalizedUpdate processes the JSView.pendingElementUpdates_ list. Performs a local Element update for each of its entries.
12. JSViewAbstract::ComponentToElementLocalizedUpdate implements local Element updates except for ForEach and If..Else case. Its processing in two steps:
13. Since the elmtId refers to the _main_ Element, but the Component refers to the outmost _wrapping_ Component, matching pairs for updating need to be found first. ViewStackProcessor::UnwrapComponentRecurseively traverses the Components until finding the _main_ Component child. 
14. Afterwards JSViewAbstract::ComponentToElementLocalizedUpdate sets to component on the Element and calls Element::Update(). It does so starting from the main Component + main Element pair until the outmost wrapping Component. 

> Ideas how to improve:
> ViewStackProcessor::UnwrapComponentRecurseively traverses the Components until finding the _main_ Component child. It has an issue with CoverageComponent that has more than one child. First child seems to always be the wrapping Component, though. UnwrapComponentRecurseively  could be saved if elmtId did not refer to the _main_ element but to the _outmost_ wrapping Element. Then Component to Element update could start right away. Traverse in parent -> child direction.
> Do not use Element::Update but use the same Element::UpdateChild(...) as in baseline. TODO What function does baseline use here?
> Possibly even first traverse from the outmost Element to parent, and do a parentElement->UpdateChild(...)
> Stop the update process when reaching the main Element. Update that Element but do not process children (if and ForEach to be investigated exceptions).


## The special case of `If ... else if .. else ..'

 
### Example: eDSL

The following fragment of a build function with if .. else if condition:

```Typescript
    if (this.toggle1) {
        Text("If 1 positive")
            .width(1 * this.size).height(1 * this.size)
    }  else if (this.toggle2) {
        Row() {
            Text("if 2")
            Text("positive")
        }
         .width(1 * this.size).height(1 * this.size)
    } else if (this.toggle3) {
        Column() {
            Text("if 3")
            Text("positive")
        }
          .width(1 * this.size).height(1 * this.size)
    } 
```

### Baseline render

The fragment of the render function in baseline.
Note the use of If.branchId. This id is synced from the IfComponent to IfElement.
In next render, comparing current and previous branchId tells if the branch has changed.
In that case the children are all new.

```TypeScript
    If.create();
    if (this.toggle1) {
      If.branchId(0);
      Text.create("If 1 positive");
      Text.width(1 * this.size);
      Text.height(1 * this.size);
      Text.pop();

    }
    else if (this.toggle2) {
      If.branchId(1);
      Row.create();
      Row.width(1 * this.size);
      Row.height(1 * this.size);
      Text.create("if 2");
      Text.pop();
      Text.create("positive");
      Text.pop();
      Row.pop();

    }
    else if (this.toggle3) {
      If.branchId(2);
      Column.create();
      Column.width(1 * this.size);
      Column.height(1 * this.size);
      Text.create("if 3");
      Text.pop();
      Text.create("positive");
      Text.pop();
      Column.pop();

    }

    If.pop();
```

### Example modified Transpiled TS

The fragment of the (initial) render function.

Take note that the entire if .. else if clause is wrapped inside a partial update lambda function.
Solution from baseline is pretty much unchanged. If.branchId is still used to track when branch changes.
When branch changes an initial / full render happens and a full Component tree is generated.

Local Sync of If deletes the old child Elements of IfElement and creates new Elements from that Component tree.
See JSIfElse::ComponentToElementLocalizedUpdate(). Discussion continues below ...

```TypeScript
      If.create();
      if (this.toggle1) {
        If.branchId(0);


        this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text elmtId " + elmtId + " start  ....");
          ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
          Text.create("If 1 positive");
          Text.width(1 * this.size);
          Text.height(1 * this.size);
          if (!isInitialRender) {
            Text.pop();
          }
          ViewStackProcessor.StopGetAccessRecording();
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text " + elmtId + " - done");
        });
        Text.pop();

      }
      else if (this.toggle2) {
        If.branchId(1);


        this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Row elmtId " + elmtId + " start  ....");
          ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
          Row.create();
          Row.width(1 * this.size);
          Row.height(1 * this.size);
          if (!isInitialRender) {
            Row.pop();
          }
          ViewStackProcessor.StopGetAccessRecording();
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Row " + elmtId + " - done");
        });



        this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text elmtId " + elmtId + " start  ....");
          ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
          Text.create("if 2");
          if (!isInitialRender) {
            Text.pop();
          }
          ViewStackProcessor.StopGetAccessRecording();
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text " + elmtId + " - done");
        });
        Text.pop();


        this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text elmtId " + elmtId + " start  ....");
          ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
          Text.create("positive");
          if (!isInitialRender) {
            Text.pop();
          }
          ViewStackProcessor.StopGetAccessRecording();
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text " + elmtId + " - done");
        });
        Text.pop();

        Row.pop();

      }
      else if (this.toggle3) {
        If.branchId(2);


        this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Column elmtId " + elmtId + " start  ....");
          ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
          Column.create();
          Column.width(1 * this.size);
          Column.height(1 * this.size);
          if (!isInitialRender) {
            Column.pop();
          }
          ViewStackProcessor.StopGetAccessRecording();
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Column " + elmtId + " - done");
        });



        this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text elmtId " + elmtId + " start  ....");
          ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
          Text.create("if 3");
          if (!isInitialRender) {
            Text.pop();
          }
          ViewStackProcessor.StopGetAccessRecording();
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text " + elmtId + " - done");
        });
        Text.pop();


        this.observeComponentCreation((elmtId: number, isInitialRender: boolean) => {
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text elmtId " + elmtId + " start  ....");
          ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
          Text.create("positive");
          if (!isInitialRender) {
            Text.pop();
          }
          ViewStackProcessor.StopGetAccessRecording();
          console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for Text " + elmtId + " - done");
        });
        Text.pop();

        Column.pop();

      }
      else {
        /* no else in source, inform branchId chg. */
        If.branchId(3);
      }

      if (!isInitialRender) {
        If.pop();
      }
      ViewStackProcessor.StopGetAccessRecording();
      console.log("Partial update: " + (isInitialRender ? 'Initial render' : 'Rerender') + " for If " + elmtId + " - done");
    });
    If.pop();
```

### Localized Element update

Baseline analysis

The case Column > IFElement > children ....

Element (base of ColumnElement)::UpdateChildWithSlot(IFElement, IfComponent, slot, renderSlot)
IfElement::CanUpdate() returns false because of branchId change.
Disposes IFElement and creates _new_ IfElement from IfComponent, process continues with children:
- to dispose calls DeactivateChild(for each child of IfElement)
- to create new InflateComponent(new IFComponent, slot, renderSlot);

In short, starting creation of the IfElement re-render of If and first render of If follow the same code path.

Localized Element update design 

Guiding principle: Use as much as possible the same code path as baseline

This is the current implementation of localized update for If.
It works in all cases where the parent of if is Column, Row, Stack.

The critical line in this function is `ifElseParentElement->UpdateChildWithSlot(ifElseElement, ifElseComponent, ...)`.
This function does the same as for baseline (see above). 

```Cpp
void JSIfElse::ComponentToElementLocalizedUpdate(
    const RefPtr<Component>& component, RefPtr<OHOS::Ace::Element>& element)
{
    RefPtr<IfElseElement> ifElseElement = AceType::DynamicCast<IfElseElement>(element);
    if (!ifElseElement) {
        LOGE("%s is not a IfElseElement. Internal Error!", AceType::TypeName(element));
        return;
    }

    RefPtr<IfElseComponent> ifElseComponent = AceType::DynamicCast<IfElseComponent>(component);
    if (!ifElseComponent) {
        LOGE("%s is not a IfElseComponent. Internal Error!", AceType::TypeName(component));
        return;
    }

    if (ifElseComponent->BranchId() == ifElseElement->BranchId()) {
        LOGD("Unchanged branchId. No updates to be done.");
        return;
    }

    LOGD("localied updte starting ...");

    // even though the ifelement will be deleted, do not unregiter it
    // because new Element with same elmtId will be created
    ElementRegister::GetInstance()->RemoveElementSilently(ifElseElement->GetElementId());
    ifElseElement->UnregisterChildrenForPartialUpdates();
    
    // get the Row, Column, Grid, List etc parent Element
    auto ifElseParentElement = ifElseElement->GetElementParent().Upgrade();  

    auto ifElementRenderSlot = ifElseElement->GetRenderSlot();

    
    ifElseParentElement->UpdateChildWithSlot(ifElseElement, ifElseComponent, ifElseElement->GetSlot(), ifElseElement->GetRenderSlot());

    // their can be sibling Eleements of IfElement. Rendernodes generated by IFElement children and these sibling Elements' RenderNodes 
    // become siblings.  When changing If branch number of RenderNodes   Therefore, their renderSlot's require updating
    // ideally, ifElseParentElement->UpdateChildWithSlot( .. ) does all the work, if not manual update to render slots might be needed.
    ifElseParentElement->ChangeChildRenderSlot(ifElseElement, ifElementRenderSlot, /* effectDescendant */ true);

    // Unsure requesting layout is needed
    ifElseParentElement->GetRenderNode()->MarkNeedLayout();

    // this does not lead to correct ordering
    // ifElseElement->SetNewComponent(ifElseComponent);
    // ifElseElement->Update();
    // note: special implementation IFElseElement::UpdateChildren(..)
    // is required because MultiComposedChildren::UpdateChildren expects number of children to remain the same
    // or then MultiComposedChildren::UpdateChildrenForRebuild
    //
    // ifElseElement->UpdateChildren(ifElseComponent->GetChildren());
    // ifElseElement->SetNewComponent(nullptr);
    LOGD("Doing a deep update on IfElseElement - DONE");
}
```


works for:
* If inside Column, test cases ets/partUpdIf*.ets
* If inside Row, test case ets/partupdRowIfElse.ets
* If inside Stack, test case ets/stackAndtextInputBuiltin.ets

remaining issues:
* Grid (conditional GridItem): Conditional GridItems are added or removed, but newly added GridItems are appended to the end of the Grid (i.e. the order of GridItems is wrong after update). testcase ets/partUpdGridIfElse.ets
* If inside Swiper as well, updates not shown, not even in wrong order. test case ets/swiperBuiltinIf.ets
* If inside List (conditional ListItem): ListItem is not shown, not even in wrong order. test case  ets/partUpdListIfSimple.ets 

As we can see, UI updates do not always work as expected. This is the fault of the parent container Element, in which If is used.
Grid, List, and Swiper are not capable to handle insertion and deletion of individual children. 

needed new functions
* GridElement:UpdateChildWithSlot(ifElement, ifComponent, slot, renderSlot)
* SwiperElement:UpdateChildWithSlot(ifElement, ifComponent, slot, renderSlot)
* ListElement:UpdateChildWithSlot(ifElement, ifComponent, slot, renderSlot)

Number and type of children of if changes. Best is probably to do what baseline does and to delete theold IfEleemnt and its children and to replace with new one.

I will still detail ForEach case. From the existing implementation, I can tell already we need functions for Grid, List, Swiper to
* insert new child item Component at given slot (in the middle)
* delete child item Element at slot
* to move child item Element from given slot to new slot without having to create a Component.


## The special case of `ForEach`
 
The core ideas of partial update for ForEach are
* the item gen function transforms an array item to Components that create or update child Element of ForEach.
* the id function uniquely identifies and array `item id(item1)==id(item2)` implies for sure `item1==item2`. however, `item1==items2` does not imply `id(item1)==id(item2)`.
* if the obtained array item id is unchanged since previous render, then the Component generated for that array item will be the same as well. Hence, that array item does not need to be re-rendered.

The framework computes the id for each array item. Stores in own array ('IdArray'). ForEachElement memorizes this array until next render. JSForEach::GetIdArray gets it from ForEachElement, JSForEach::SetIdArray assignt the IDArray to the ForEachComponent from where it snc's to the ForEachElement.

Comparing the previous render IdArray with the current IdArray identifies:
* added array item -> need to execute ItemGenFunction, obtain Component, and add to ForEachComponent (adds only Component for newly added array item!)
* deleted array item -> remove corresponding child Element of ForEachElement
* moved array item -> change the slot & renderSlot of  corresponding child Element of ForEachElement, no need to compute a Component because array item value is unchanged.

Function ForEachElement::UpdateWithComponent(ForEachComponent) is where this magic happens. 
It requests the following three operations on the Element tree:
* add new child element: Element::InflateComponent(new child Component, slot, renderSlot);
* remove child Element: ComposedElement::UpdateChild(old child Element, /* no new child component */ nullptr);
* move child Element: Element::ChangeChildSlot(unchanged child Element, /* changed */ slot); and Element::ChangeChildRenderSlot(unchanged child Element, /* changed */ renderSlot, true);

 ```Cpp
void ForEachElement::UpdateWithComponent(const RefPtr<Component>& newComponent1)
{
    LOGD("Updadating ForEachElement (elmtId: %d)....", GetElementId());
    RefPtr<PartUpd::ForEachComponent> newFEComp = AceType::DynamicCast<PartUpd::ForEachComponent>(newComponent1);
    std::list<std::string> newIds = newFEComp->GetIdArray();
    std::list<std::string> oldIds = GetIdArray();
    auto additionalChildComps = newFEComp->GetChildren();
    auto oldChildElementsRef = GetChildren();
    std::list<RefPtr<Element>> oldChildElements(oldChildElementsRef); // make a copy of the list
    oldChildElements.sort(compare_slots);

    ACE_DCHECK((oldIds.size() == oldChildElements.size()) &&
               "Number of IDs generated during previous render and number of ForEach child Elements must match");

    auto firstChildElement = GetChildren().begin();
    int renderSlot = (*firstChildElement)->GetRenderSlot(); // FIXME, case of no existing children TDOO
    int32_t slot = (*firstChildElement)->GetSlot();
    int32_t oldIndex = -1;
    int additionalChildIndex = 0;

    // add new items, move items
    for (auto newId : newIds) {
        if ((oldIndex = indexOf(oldIds, newId)) == -1) {
            // found a newly added ID
            // insert component into 'slot'
            auto newCompsIter = additionalChildComps.begin();
            std::advance(newCompsIter, additionalChildIndex++);
            LOGD("Elmt with New arr-id '%s', inserting to slot %d / renderSlot %d", newId.c_str(), slot, renderSlot);
            InflateComponent(*newCompsIter, slot, renderSlot);
        } else {
            // the ID was used before, only need to update the child Element's slot
            auto oldElementIter = oldChildElements.begin();
            std::advance(oldElementIter, oldIndex);
            LOGD("Elmt with arr-id %s retained, update its slot %d->%d / renderSlot %d->%d", newId.c_str(),
                (*oldElementIter)->GetSlot(), slot, (*oldElementIter)->GetRenderSlot(), renderSlot);
                ChangeChildSlot(*oldElementIter, slot);
                ChangeChildRenderSlot(*oldElementIter, renderSlot, true);

                LOGD("MarkNeedLayout ...");
                // FIXME pointer checks!
                (*oldElementIter)->GetRenderNode()->GetParent().Upgrade()->MarkNeedLayout();
                LOGD("MarkNeedLayout - done");
        }
        slot++;
        renderSlot++;
    }

    // delete items
    oldIndex = 0;
    for (std::string oldId : oldIds) {
        // check if oldId still in newIds array
        if (indexOf(newIds, oldId) == -1) {
            // the ID is no longer used, delete the child Element
            auto oldElementIter = oldChildElements.begin();
            std::advance(oldElementIter, oldIndex);
            LOGD("Array Id %s no more used, deleting %s(%d)", oldId.c_str(), AceType::TypeName(*oldElementIter),
                (*oldElementIter)->GetElementId());
            UpdateChild(*oldElementIter, /* no new child component */ nullptr);
        }
        oldIndex++;
    }

    SetIdArray(newFEComp->GetIdArray());
    Update();
    SetNewComponent(nullptr);

#ifdef ACE_DEBUG

    LOGD("ForEachElement children after change:  ");
    for (auto childElmt : GetChildren()) {
        LOGD("   ... slot %d / renderSlot %d.", childElmt->GetSlot(), childElmt->GetRenderSlot());
    }
    LOGD("  ... total children Elements: %llu .", GetChildren().size());
#endif

    LOGD("Updadating ForEachElement done");
}
```

 