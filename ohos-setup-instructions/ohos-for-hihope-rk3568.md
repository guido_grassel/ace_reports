# OHOS Setup, Build, and Flash Instructions for OHOS Development Board HiHope rk3568

# Introduction

The OHOS Development Board HiHope rk3568 is available in China only. 
Information about its capabilities and asic setup, see (in Chinese, translatable): 
https://gitee.com/openharmony/device_hihope

This guide assumes you have a working environment for ArkUI preview on Windows alreadu.

# Additional Setup for Ubuntu 20

In addition that what has been installed for Windows build already:

```
sudo apt install genext2fs liblz4-tool libc-bin libcurl4 git libfl2 libfl-dev libssl-dev
```


# Setup rk3568 Driver and Flash Tool on Windows

1. Download the whole git repository as archive from here: 
https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200/%E7%83%A7%E5%86%99%E5%B7%A5%E5%85%B7%E5%8F%8A%E6%8C%87%E5%8D%97/windows (I used the Clone or Download > Download ZIP)

2. Copy to directory `docs-master\HiHope_DAYU200\烧写工具及指南\windows` to an empty directory without (important) Chinese characters, 
in my cased `D:\RKDevTool`

3. Inflate DriverAssistent.zip archive and install . 

4. Once done the DriverAssistent directory and Zip file can be deleted to obtain a clean app directory for RKDevTool

5. In the same directory change the line `Selected=1` in `config.ini` to `Selected=2` to switch to English for RKDevTool

```
[Language]
Kinds=2
Selected=2
LangPath=Language\

Lang1File=Chinese.ini
Lang1FontName=宋体
Lang1FontSize=9

Lang2File=English.ini
Lang2FontName=Arial
Lang2FontSize=9
```



# Get the code and prebuilds

Same as for Windows.

# Build Flash Images

```
./build.sh --product-name rk3568 --ccache
```

The build results will be in `out/rk3568/packages/phone/images`. You should have the following images:
`MiniLoaderAll.bin`
`boot_linux.img`
`parameter.txt`
`resource.img`
`system.img`
`uboot.img`
`updater.img`
`userdata.img`
`vendor.img`

Copy all these to an easily-accessible path on Windows, in my case in my cased `D:\RKDevTool\images`.

Prepare the development board. 
   1. Connect the blue USB-A to USB-A cable to the USB-OTG port on the board (written with small letters on the board itself)
   2. Connect the grey cable ("DEBUG" on the board)
   3. Locate the 6 buttons (Power, Reset, Recovery, Volume Up, Down etc.) which are placed in a 2x3 fashion.
   4. Hold **RECOVERY** button as you plug in the AC power adapter to the board. Release the button after 2-3 seconds

Run `RKDevTool.exe`. If you completed the above steps correctly, you should see `"Found one LOADER device"` on the bottom part of the tool. If you see `"Found one MASKROM device"`, repeat the steps in 4.

On the first tab of the tool, click on `...` next to each image and point it to the correct location
Click "Run" and wait for it to finish
Once it is done, your development board will restart and boot into the OpenHarmony home view. 

