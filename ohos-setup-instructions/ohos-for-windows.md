
# OHOS ArkUI Preview for Windows

Instructions for ArkUI Preview for Windows.
Prerequisites, getting the code, building, and running.

## Build Computer with Ubuntu

Non-virtualized computer with >200GB SSD disk, 16-32GB RAM, 
and 8 physical cores is recommended. Windows Preview/SDK build takes ~12-15min.

People have managed to build on Windows WSL and 16GB, but then the build takes 
hours.

A computer with direct Internet connection is highly recommended.
Getting the code from behind the Huawei firewall/proxies requires a complicated setup,
and often gets interrupted with connection errors. Some people spent days and could 
not get a complete fresh tree,

OHOS build script only support Ubuntu 20.04 and 18.04 LTS.
Given v18 is very old, we recommend 20.04 LTS.

We have tried and ran into issues with dependencies on Ubuntu 22.04 LTS.


## Install needed packages

```
sudo apt update
sudo apt install binutils git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby openssl libncurses5
sudo apt install openjdk-11-jre-headless
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 1
sudo apt-get install python3-setuptools python3-pip -y
sudo pip3 install --upgrade pip
```

## Prepare to access 'gitee.com'

1. Create a SSH key:
```
ssh-keygen -t rsa -b 4096 -C "your_email@huawei.com"
``` 

2. Register to gitee.com. Add the public key you just created. (`cat /home/<user>/.ssh/id_rsa.pub` and copy)

## Make yourself known to git

```
git config --global user.name "Your Name"
git config --global user.email "your_email@huawei.com"
git config --global credential.helper store
```

## Install repo tool:

```
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo
sudo mv repo /usr/local/bin/repo
chmod a+x /usr/local/bin/repo
pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
```

# Get the latest code and prebuilds

```
mkdir ~/sources/<directory-name-recommended-with-todays-date>
cd ~/sources/<directory-name-recommended-with-todays-date>

repo init -u git@gitee.com:openharmony/manifest.git -b master --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'

bash build/prebuilts_download.sh
```

Repo man pages: https://source.android.com/setup/develop/repo

If you are behind Huawei firewall/proxy consider getting a ready source+prebuilds tar 
by requesting from the author.


# Setup nodejs

Sym Links for node & friends - It is recomended to use node and npm from prebuilds to avoid multiple version in the installation.

```
cd /usr/local/bin
sudo ln -s <abs path workspace>/prebuilts/build-tools/common/nodejs/node-v12.18.4-linux-x64/bin/npm .
sudo ln -s <abs path workspace>/prebuilts/build-tools/common/nodejs/node-v12.18.4-linux-x64/bin/node .
sudo ln -s <abs path workspace>/prebuilts/build-tools/common/nodejs/node-v12.18.4-linux-x64/bin/npx .
```

# Build the code

```
./build.sh --export-para PYCACHE_ENABLE:true --product-name ohos-sdk --ccache --gn-args enable_glfw_window=true
```

if things go well, you will see a message ending with `=====build  successful=====`
If not, open the build log file and analyse. Keep in mind this is a multithread build, the error 
messages can be spread.

# Prepare Windows computer

Recommendation is to install cygwin for Windows, and to get the rsync file sync utility (note: not installed by default!)
rsync supports differential downloads on binary files. 

You must create _exaxtly_ these directories on drive D:

```
d:\Workspace\AceWindowsTestApp
d:\Workspace\AceWindowsTestApp\preview\js\default_2.0
```

The former is for the Preview executable the latter for the transpiled ACE application.

# Transfer executable files from Ubuntu to Windows

People use many different solutions. 
Mine is a script `get.sh` placed in the `AceWindowsTestApp` directory,
run in cygwin bash.

Paste and modify the following lines to `get.sh`:

```
#! /bin/bash

RSYNC="rsync --progress"

#own user id and host name or password
HOST=user@host

# the root directy, i.e. tghe directory where you executed the build.sh command:
BASE="~/sources/baseline-march26"


echo "Fetching: =========================="
echo "from tree: $BASE"
echo "Fetching: =========================="
echo ""

# Windows EXE and LIBS
$RSYNC $HOST:$BASE/out/sdk/mingw_x86_64/common/common/* .
```

# Transpile and Run the ArkUI sample application

## Preparing the command line transpiler from HQ 

Setup `ace-ets2bundle` that comes with the source tree:

```
cd developtools/ace-ets2bundle/compiler
npm install
npm run build
```

Read its README.md

## Transpile sample project

Transpile the sample project in `./sample` directory.
The transpilation result is placed into folder `./sample/build` .

```
$ npm run compile
```

Browse the generated files. Note the `manifest.json` file and the 
contents of the `./pages` directory

the `manifest.json` file pages array defines the JS file names of pages included in the app.
The first entry is the startup page. at time of writing this was `pages/index.js` .


## Copy transpilation result to Windows

Copy (recursively) the contents of folder `./sample/build` (not the folder itself)
to `d:\Workspace\AceWindowsTestApp\preview\js\default_2.0` on Windows. Hence the 
`manifest.json` file and `pages` directory are placed inside the `default_2.0` directory.

If you use above `get.sh` script this copy operation is a good addition to the script.

# Run ArkUI application in Preview 

You have copuied the executable files and the transpield ArkUI applicatiom.
You are good to run your first ArkUI app!

```
cd d:\Workspace\AceWindowsTestApp
ace_test_windows_tablet.exe 2.0 
```

Congratulatioons!

Running Preview also works from cygwin shell.

Once this works, head back to the ets2bundle transpiler, create your own project (see its README.md),
run again.

This Chinese document also includes some info about the Windows build: 
https://gitee.com/openharmony/ace_ace_engine/wikis/Windows%E9%A2%84%E8%A7%88%E7%BC%96%E8%AF%91%E9%AA%8C%E8%AF%81

# What next ?

## Compiler NT 

Compiler NT is an internal tool we use at Finland RnD center, in parts an alternative to ace-ets2nbundle.
Ask access thru private channels.
Its git repository needs to be manually placed to the workspace root directory.

Read its REAME file.

ArkUI test apps are in ace-compiler-nt/ets.
Transpiled JS file is placed into ace-compiler-nt/out directory.
Copy just the one output file <mytest>_debug.js to 
`d:\Workspace\AceWindowsTestApp\preview\js\default_2.0\pages\index.js`

Best to add this step to your `get.sh` script.

`pages\index.js` is the startup page JS file (defined in the manifest.json by placing it first into the pages list). Your copied JS file will thereby be loaded when Preview starts.

## DevEcoStudio

Download and experiment with the latest beta of DevEcoStudio from 
https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta

At the time of writing the latets beta was build 3.0.0.900 .

## Browse and Tweak the ArkUI framework sources

The ArkUI framework implementation is located in 
`foundation/arkui/ace_engine/frameworks`. The main directories therein:
* `bridge/declarative_frontend/` - the ArkUI declarative frontend implementation
* 'core/pipeline/`
* 'core/components/`
* `core/components_v2'  - three places where Component, Element, and Render classes can be found.

> to be continued










