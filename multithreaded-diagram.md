
## Sequence diagrams with multi-threaded optimization
Legend:

    🟦 UI thread
    🟥 JS thread

    ——— native execution
    --- Javascript execution

### On page load sequence

```plantuml
@startuml
skinparam shadowing false
skinparam BoxPadding 10
entity App
queue TaskExecutor
scale 0.9
title Page load

activate App
create JSView 
App --[#red]> JSView: new 
note right 
This is the entry component
end note
App --[#red]> JsLoadDocument: loadDocument callback in native
JsLoadDocument -[#red]> DirtyViewContainer: Enter()
activate DirtyViewContainer
TaskExecutor <[#red]- DirtyViewContainer: (1) SetBlockLayout(true) @ UIThread
...
TaskExecutor -[#blue]> PipelineContext: (1) SetBlockLayout(true)
...
JsLoadDocument -[#red]> JSView : CreateComponent
create ComposedComponent
JSView -[#red]> ComposedComponent: new
JSView -[#red]> ComposedComponent : SetElementFunction
JSView -[#red]> JsLoadDocument : returns OHOS::Ace::Component
JsLoadDocument -[#red]> JSAcePage : SetRootComponent
JsLoadDocument -[#red]> DirtyViewContainer: DelayedExit
deactivate DirtyViewContainer
TaskExecutor <[#red]- DirtyViewContainer: (2) Exit @ JSThread
deactivate App
@enduml
```

```plantuml
@startuml
skinparam shadowing false
skinparam BoxPadding 10
entity App
queue TaskExecutor
participant View #FF69B4
title Processing components
scale 0.9
TaskExecutor -[#red]> DirtyViewContainer: (2) Exit
loop Dirty views queue
DirtyViewContainer -[#red]> JSView: DoUpdate
JSView -[#red]> ViewFunctions : executeLifetimeAndRender()
ViewFunctions --[#red]> App: render() (build function)
activate App
App --[#red]> View: Create
View -[#red]> JSView: CreateComponent
create ComposedComponent
JSView -[#red]> ComposedComponent: new
JSView -[#red]> ComposedComponent: SetElementFunction()
JSView -[#red]> ViewStackProcessor: Push(this)

return

JSView -[#red]> ViewStackProcessor: Finish()
hnote over JSView
JSView stores the resulting component from ViewStackProcessor::Finish
endhnote
TaskExecutor <[#red]- DirtyViewContainer: (3) Element_Rebuild(force = true) @ UIThread
end
TaskExecutor <[#red]- JSView: (4) SetBlockLayout(false) @ UIThread
...
TaskExecutor -[#blue]> PipelineContext: SetBlockLayout(false)
@enduml
```

```plantuml
@startuml
!pragma useVerticalIf on
skinparam shadowing false
skinparam BoxPadding 10
queue TaskExecutor
title Signaling the elements on the UI thread to rebuild 
scale 0.9
TaskExecutor -[#blue]> PipelineContext : called from OnIdle, Vsync\n or (3) Element_Rebuild
PipelineContext -[#blue]> PipelineContext : FlushBuild()
PipelineContext -[#blue]> ComposedElement : Rebuild()
ComposedElement -[#blue]> ComposedElement : PerformBuild()
ComposedElement -[#blue]> ComposedElement : CallRenderFunction()
note right 
   lambda at JSView::CreateComponent
end note
alt#Silver #ADD8E6 If component tree is ready
   JSView -[#blue]> ComposedElement: UpdateChild() with stored component tree
else #pink If FlushLayout is not blocked
    TaskExecutor <[#blue]- JSView: Prepare component @ JSThread
    ...
    TaskExecutor -[#red]> JSView: Prepare component
    JSView -[#red]> JSView: InternalRender()
    TaskExecutor <[#red]- JSView: Element_MarkDirty @ UIThread
else #lightgreen Else
   JSView -[#blue]> ComposedElement: UpdateChild() with dummy BoxComponent
end
PipelineContext -[#blue]> PipelineContext : FlushLayout()
note right
At this point the FlushLayout is unblocked
end note
PipelineContext -[#blue]> PipelineContext : FlushRender()

@enduml
```

### On user event execution

```plantuml
@startuml
skinparam shadowing false
skinparam BoxPadding 10
actor User
queue TaskExecutor
participant View #FF69B4
title User action execution
scale 0.9

User -[#blue]> JSInteractableView : a click event gets recognized
note right
lambda at JSInteractableView::JsOnClick
end note
TaskExecutor <[#blue]- JSInteractableView : (5) Call JS click registered method @ JSThread
TaskExecutor -[#red]> JSInteractableView: (5) Call JS click registered method
note right
lambda at JSInteractableView::JsOnClick
end note
JSInteractableView -[#red]> DirtyViewContainer: Enter()
activate DirtyViewContainer
TaskExecutor <[#red]- DirtyViewContainer : (6) SetBlockLayout(true) @ UIThread
...
TaskExecutor -[#blue]> DirtyViewContainer: (6) SetBlockLayout(true)
DirtyViewContainer -[#blue]> PipelineContext: SetBlockLayout(true)
...
DirtyViewContainer -[#red]> JSInteractableView: return true;
JSInteractableView -[#red]> JsClickFunction: Execute(info)
activate JsClickFunction
?--[#red]> View: **A state changes**
group This can execute as often as many views that need updating
View --[#red]> JSView : MarkNeedUpdate()
JSView -[#red]> DirtyViewContainer: Push(this)
end
deactivate JsClickFunction
JSInteractableView -[#red]> DirtyViewContainer: DelayedExit()
deactivate DirtyViewContainer
TaskExecutor <[#red]- DirtyViewContainer: (2) Exit @ JSThread
hnote over TaskExecutor 
Refer back to "Processing components" diagram 
for the execution that happens beyond this point
end note
@enduml
```

