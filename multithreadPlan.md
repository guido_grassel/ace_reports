# Multithreaded Solution Plan

# Plan Summary

Development and functionality testing is done on Windows preview app, and benchmarking on AOSP test app from Sunfei.

1. Solution development and testing phase
    - Separation of execution of JS build() function on JS thread while UI thread builds the elements from components in parallel. 
    - Layout done on UI thread after JS execution and element building is done.
    - COVID19 test app functionality and benchmarking (execution time on load decreased up to 25% and user interaction up to 33%)
    - **IN PROGRESS** Verification of functionality of individual framework component. What other components and attribute functions call back from ElementRender nodes to JS? 
    - **IN PROGRESS** The major remaining issue is with LazyForEach inside List, Grid Swiper.
    - Investigate possible crossing of JS/UI thread boundaries by components other than Composed, List, Grid, ForEach/LazyForEach, IfElse 

2. Functionality testing of benchmark applications (Taobao, Weixin, Toutiao, Shortvideo)
    - **IN PROGRESS** Taobao landing page works with the exception of LazyForEach inside Grid. 
    - Weixin, Toutiao, Shortvideo are yet to be analyzed.

3. Functionality testing of OHOS system apps inside `applications/standard` folder ('settings', 'photo' )
    - co-work HQ framework, HQ app developers, and Finland team.

4. Submit a pull-request, code review, merging to master.

## Questions

1. Compile benchmark applications Taobao, Weixin, Toutiao, Shortvideo, *which compiler, where is it and documentation needed how to use?*
2. We are unsure if we get mutithread or partial update ready first. Integrating the two solution is an extra step, needs to planed for either multithread or partial update, whatever will finish second.


# Testing status

| Icons| Status |
| :--: | :----- |
| :white_check_mark: | partial update is OK | 
| :no_entry: | partial update still has issues. See comment what the issue is | 
| BL :interrobang: | baseline has issue, and/or insufficient/unclear documentation. We can not test on partial update. Follow link to issue on gitee.com |


## Basic Components

| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| [Blank](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-blank.md) | ets/partUpdBlank.ets | :white_check_mark: | | |
| [Button](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-button.md) | ets/partUpdButton.ets </br> ets/partUpdButtonLabel </br> ets/partUpdButtonTextChild.ets| :white_check_mark: | | |
| [DataPanel](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-datapanel.md) | ets/partUpdDataPanel.ets | :white_check_mark: || |
| [DatePicker](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-datepicker.md) | ets/partUpdDatePicker.ets | BL :interrobang: | **Problem in baseline:**  setting 'selected' date and lunar mode fails | [DatePicker doesn't select given date](https://gitee.com/openharmony/ace_ace_engine/issues/I552HV)
| [Divider](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-divider.md) | ets/partUpdDivider.ets | :white_check_mark: || |
| [Gauge](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-gauge.md) | ets/partUpdGauge.ets | :white_check_mark: || |
| [Image](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-image.md) | ets/partUpdTabsBuilder.ets | :no_entry: | Image doesn't change when switching between tabs, the add icon doesn't show up at all | |
| [ImageAnimator](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-imageanimator.md) | ets/partUpdImageAnimator.ets | BL :interrobang: | **Problem in baseline:**  ImageAnimator works a bit unreliably at best, switching states, direction and amount of iterations can get stop or show images in wrong order, but it continues eventually when clicking the buttons enough. Loads the second image on startup. </br>  **In multithread**  it seems to be working a bit better, but to be re-evaluated when baseline is fixed. | [ImageAnimator is not reliable](https://gitee.com/openharmony/ace_ace_engine/issues/I4ZZY3?from=project-issue) |
| [LoadingProgress](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-loadingprogress.md) | ets/partUpdLoadingProgress.ets | :white_check_mark: | | |
| [Marque](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-marquee.md) | ets/partUpdMarquee.ets  | :no_entry: | Switching message and direction works, slowing down works, but when stopped it doesn't start running anymore. | |
| [Progress](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-progress.md) | ets/partupdProgress.ets | :white_check_mark: | | |
| [QRCode](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-qrcode.md) | ets/partUpdQRCode.ets | :white_check_mark: || |
| [Radio](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-radio.md) | ets/partUpdRadio.ets | :white_check_mark: |
| [Rating](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-rating.md) | ets/partUpdRating.ets | :white_check_mark: || |
| [Search](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-search.md) | ets/partUpdSearch.ets | BL :interrobang: | **Problem in baseline:**  The input string is lost when pressing Search, it shows the placeholder text again. If you add a letter to the input string, it shows again correctly. </br> **Multithread** works identically. | [Search component loses input](https://gitee.com/openharmony/ace_ace_engine/issues/I552II) |
| [Select](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-select.md) | ets/partUpdSelect.ets | BL :interrobang: | **Problem in baseline:**  both at toggling text/index and crashes at selecting an item. | Probably fixed in next rebase! |
| [Span](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-span.md) | ets/spanBuiltin.ets | :white_check_mark: | | |
| [Slider](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-slider.md) | ets/partupdSlider.ets | :no_entry: | Value of each slider can be changed, but the other sliders don't change at the same time. | |
| [Stepper](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-stepper.md) </br> [StepperItem](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-stepperitem.md) | ets/partUpdStepperIf.ets | BL :interrobang: |  **Problem in baseline:**  Can not add StepperItems conditionally. </br>  **Multithread:**   works identically. | [Conditional Stepper item](https://gitee.com/openharmony/ace_ace_engine/issues/I4XT42) |
| [Text](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-text.md) | many test cases that change label exist </br> ets/partUpdText.ets - tests everything on Text | :white_check_mark: || |
| [TextArea](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-textarea.md) | ets/partUpdTextArea.ets | :white_check_mark: || |
| [TextInput](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-textinput.md) | ets/partUpTextInput.ets | :white_check_mark: | | |
| [Toggle](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-toggle.md) | ets/partUpdToggle.ets | BL :interrobang: </br> :no_entry: | **Problem in baseline:**  All the toggles in the test case share the common `isOn` state. Toggling any of them on only changes that individual toggle's value. Toggling any of them off, switches all of them off. </br> **Multithread**  works differently, all toggles can be set on and off individually. | [Toggle value update](https://gitee.com/openharmony/ace_ace_engine/issues/I4W895)|
| [Video](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-media-components-video.md) | ets/partUpdVideo.ets | :white_check_mark: |  **_Video component not supported on windows_** , but can show preview images: press stop, then src and then previewUri in sequence, it will alter between the 2 images. Works the same way on both branches. | |


## Custom Components

@Component, @State, @Link, @Prop 

| Test case | What tested | Status | Notes | 
| --------- | ----------- | :----: | ----- |
| ets/partUpdBuiltinChildren.ets |  Single @Component | :white_check_mark: ||
| ets/partUpdCustomChildren.ets </br> ets/partUpdNestedCustomChildren.ets </br> ets/partUpdLink.ets </br> ets/partUpdMixedChildrenSimpleType.ets | Multiple @Component and children and grand children, @State @Link, @Prop | :white_check_mark: ||
| ets/covid19.ets | benchmark app with many @Component use @State, @Link, @Prop, @Objectlink | :white_check_mark: | |
| ets/guessTheImage_1.ets <br> ets/guessTheImage_2.ets | two versions of benchmark app with multiple @Component | :no_entry: | Can not change the image in either of the versions. Version 2 doesn't arrange the image correctly. |

## Row, Column, Stack, Flex Containers

Core containers components require testing in combination with If and ForEach, and  updating attribute functions on Containers.

Status summary:
* [Row](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-row.md) :white_check_mark:
* [Column](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-column.md) :white_check_mark: 
* [Stack](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-stack.md) :white_check_mark:
* [Flex](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-stack.md) :white_check_mark:

TODO: We should check the attribute functions and make some more tests updating those.

| Test case | What tested | Status | Notes |
| --------- | ----------- | :----: | ----- | 
| ets/partUpdIf.ets - one branch </br> ets/partUpdIfElse.ets - if else branches </br> ets/partUpdIfElseIf.ets </br> ets/partUpdIfElseIfElse.ets  - more branches | IF inside Column | :white_check_mark: ||
| ets/partUpdIfCustomComponent1.ets </br> ets/partUpdIfCustomComponent2.ets </br> ets/ partUpdIfCustomComponentNested.ets | Conditional Child: @Component inside If inside Column | :white_check_mark: | |
| ets/partUpdRowIfElse.ets | IF inside Row | :white_check_mark:  | |
| ets/partUpStackIf.ets | IF inside Stack | :white_check_mark: | |
| ets/partUpdForEachSimple.ets | ForEach inside Column | :white_check_mark: | |
| ets/partUpdForEachCustomComp.ets </br> ets/partUpdForEachObjectLink.ets - with @ObjectLink | repeated @Component: Child @Component inside ForEach inside Column | :white_check_mark: | |
| ets/partUpdForEachIf.ets | Container > ForEach > IF | :white_check_mark: |  |
| ets/partUpdFlexIf.ets | conditional child inside Flex container: Flex > IF > Text | :white_check_mark:  |  |


## Specialized Containers

| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| [AlphabetIndexer](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-alphabet-indexer.md) | ets/partUpdAlphabetIndexerIf.ets | :white_check_mark: | || 
| [Badge](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-badge.md) | ets/partUpdBadgeIf.ets | :white_check_mark: |||
| [ColumnSplit](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-columnsplit.md) | ets/partUpdColumnSplitIf.ets </br> ets/partUpdForEachColumnSplit.ets | :white_check_mark: | ||
| [Counter](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-counter.md) | ets/partUpdCounterIf.ets | :no_entry: | Toggling color and height works, but the Counter doesn't show the text items inside it at all. ||
| [GridContainer](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-gridcontainer.md) | ets/partUpdGridContainerIfSimple.ets | :white_check_mark: | ||
| [Navigator](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-navigator.md) | ets/partUpdNavigator.ets </br> ets/partUpdNavigatorDetail.ets </br> ets/partUpdNavigatorBack.ets | BL :interrobang: | **Problem in baseline:**  setting the Navigator active true or false doesn't have effect to the component, it navigates to the page even when not active. It goes index -> Detail -> Back -> index, but works only once, starting a new round shows an empty page. **Multithread** works identically. | [Navigator can not be set inactive](https://gitee.com/openharmony/ace_ace_engine/issues/I526TQ?from=project-issue)|
| [Navigation](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-navigation.md) | TODO | | ||
| [Panel](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-panel.md) | ets/partUpdPanelIf.ets | :white_check_mark: |||
| [Refresh](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-refresh.md) | ets/partUpdRefreshIf.ets | BL :interrobang: </br> :no_entry: |  **Problem in baseline:**  The component seems to want to control it's refreshing status itself even tough the `@State isRefreshing` variable is given to it from outside. The component does not update the refreshing state correctly, it never stops showing the spinner. Also modifying the `offset` and `friction` parameters from the initial values makes the usage of the component only worse. The documentation does not mention neither of the parameters affecting the size of the spinner, but they do and it's not systematic. They don't seem to be doing what they are described to do. </br> **In multithread:**  Timeout callback is not called. | Not creating any issues right now, maybe it's better when we rebase... |
| [RowSplit](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-rowsplit.md) | ets/partUpdRowSplitIf.ets </br> ets/partUpdForEachRowSplit.ets | :white_check_mark: ||
| [Scroll](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-scroll.md) | ets/partUpdScrollForEach.ets |  :white_check_mark: ||
| [ScrollBar](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-scrollbar.md) | ets/partUpdScrollBar.ets | BL :interrobang: |  **Problem in baseline:**  It is not possible to turn the scrollbar into horizontal mode, even though the Scroll and Scroller turn. </br>  **Multithread:**   works identically. | [ScrollBar can't be used in horizontal mode](https://gitee.com/openharmony/ace_ace_engine/issues/I54V77) |
| [SideBarContainer](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-sidebarcontainer.md) | ets/partUpdSideBarContainer.ets | BL :interrobang: </br> :no_entry: | **Problem in baseline:**  Width of the side bar can not be changed. </br> **Multithread:**  Toggling the control button visible also shows the side bar, that doesn't happen in baseline. Changing the width **works**. | Guido created an issue? |
| [Tabs](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-tabs.md) </br> [TabContent](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-tabcontent.md) | ets/partUpdTabsStatic.ets </br> ets/partUpdTabsBuilder.ets | :no_entry: | Switching tabs and labels works, but tab builder doesn't switch icons ||

## List

[List](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-list.md) 


| Test case | What tested | Status | Notes | Issues |
| --------- | ----------- | :----: | ----- | ------ |
| ets/partUpdListStatic.ets | attribute changes only | :white_check_mark: | Setting list backgroundcolor doesn't work, but doesn't work in baseline either | |
| partUpdListIfBranchId.ets <br> partUpdListIfLazyForEach.ets | Conditional ListItems | :no_entry: | Text items don't change with the value </br> Non-static list items don't show up | |
| partUpdListIfElseNested.ets <br> partUpdListIfSimple.ets <br> partUpdListIfElseIf.etss | Conditional ListItems | :white_check_mark: | | |
| partUpdListForEachSimple.ets | repeated ListItems, ForEach | :no_entry: | Switching ListItem or List color doesn't work, but doesn't work in baseline either ||
| partUpdListForEachCustComp.ets <br> partUpdListForEachObjectLink.ets  | repeated ListItems, using ForEach | :no_entry: | Cases don't compile with compiler_nt | |
| ets/partUpdListLazyForEachSimple.ets <br> ets/partUppdListLazyForEachCustomComp.ets | repeated ListItems, using LazyForEach | :no_entry: | In Simple case, only toggling the Column color works, no list items appear </br> CustomComp doesn't compile | |
| ets/partUpdListforEachNonLazy.ets | ListItems create lazy (shallow) by default unless inside LazyForEach | |   
| TODO | conditional repated ListItems: ForEach and LazyforEach, in combination with If |  | | |
| TODO | two forEach inside same List |  | |



## Grid

[Grid](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-grid.md) container - :warning: 

| Test case | What tested | Status | Notes |
| --------- | ----------- | ---- | ----- | 
| ets/partUpdGridStatic.ets | static list of GridItem children, child attribute updates |  | Case not found! |
| ets/partUpdGridIfElseScrolling.ets | conditional GridItems | | Case not found! | 
| ets/partUpdGridIf.ets <br> ets/partUpdGridIfElse.ets | conditional GridItems | :white_check_mark: ||
| ets/guesstheImage_2.ets | Our new benchmark app. Uses repeated GridItems with ForEach. Has no scrolling, | :no_entry: | Can't select the picture and the picture doesn't get arranged. |
| ets/partUpdGridForEachSimple.ets | Repeated GridItem, with ForEach | :white_check_mark: || 
| tbd | Repeated GridItems, with LazyForEach | | 


## Swiper 

[Swiper](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-swiper.md) - :warning:  

| Test case | What tested | Status | Notes |
| --------- | ----------- | ---- | ----- | 
| ets/swiperBuiltinStatic.ets </br> ets/swiperBuiltin.ets | Swiper with static list of children, update Swiper child attr | :white_check_mark:  | |
| ets/swiperBuiltinIf.ets </br> ets/swiperBuiltinIfElse.ets | Conditional Swiper child | :white_check_mark: | |
| ets/swiperBuiltinForEach.ets | Repeated Swiper children with ForEach | :white_check_mark:  | | 
| swiperBuiltinLazyForEach.ets | Repeated Swiper children with LazyforEach | BL :interrobang: </br> :no_entry:  | **Problem in baseline:** Some thread gets stuck and the app freezes. </br> **In multithread**  the test crashes at startup | 


## Drawing Components

| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| [Circle](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-circle.md) | ets/partUpdCircle.ets  | :white_check_mark: ||
| [Rect](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-rect.m ) | ets/partUpdRect.ets | :white_check_mark: ||
| [Ellipse](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-ellipse.md) | ets/partUpdEllipseIf.ets | :white_check_mark: ||
| [Line](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-line.md) | ets/partUpdLineIf.ets | :white_check_mark: ||
| [Path](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-path.md) | ets/partUpdPath.ets | :white_check_mark: ||
| [Polyline](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-polyline.md) | ets/partUpdPolyline.ets | :white_check_mark: ||
| [Polygon](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-polygon.md) | ets/partUpdPolygon.ets | :white_check_mark: ||
| [Shape](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-shape.md) | ets/partUpdShape.ets | :white_check_mark: | OK-ish. Moving viewport works on, but there's some flickering. **Similar to baseline**  |


## Canvas Components


| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| [Canvas](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-components-canvas-canvas.md) | ets/partUpdCanvas.ets </br> etc/partUpdCanvasDrawPrimitive.ets | :no_entry: | Can toggle color of the background, but the square doesn't show up. ||
| [Canvas](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-components-canvas-canvas.md) | ets/partUpdCanvasGradient.ets </br> ets/partUpdCanvasOffScreen.ets | :no_entry: | Gradient and offscreen doesn't show initially, but are drawn after toggle. ||
| [Canvas](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-components-canvas-canvas.md) | etc/partUpdCanvasRedrawSemantics.ets | :no_entry: | Flags don't show up at all. ||


## Popup
| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| Popup | ets/partUpdPopup.ets | :no_entry: | It does show popups, but unreliably. It can open two at the same time and get stuck with them so that they can't be closed anymore. Doesn't print callback prints. ||

## Callbacks

Callback tests are included in the normal component test cases except for Grid and List that have their own dedicated tests. 

| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| AlphabeticIndexer | ets/partUpdAlphabeticIndexerIf.ets | :white_check_mark: | ||
| Grid | ets/partUpdGridCallback.ets | :white_check_mark: | ||
| List | ets/partUpdListCallbacks.ets | :no_entry: | Callback prints not found ||
| Swiper | ets/swiperBuiltin.ets | :no_entry: | Callback prints not found ||
| Image | ets/partUpdImage.ets | :no_entry: | Callback prints not found ||
| Marguee | ets/partUpdMarquee.ets | :no_entry: | Callback prints not found ||
| Panel | ets/partUpdPanelIf.ets | :no_entry: | Callback prints not found ||
| Rating | ets/partUpdRating.ets | :no_entry: | Callback prints not found ||
| Scroll | ets/partUpdScrollForEach.ets | :no_entry: | Callback prints not found ||
| Search | ets/partUpdSearch.ets | :no_entry: | Callback prints not found ||
| Select | ets/partUpdSelect.ets | :no_entry: | Callback prints not found ||
| Slider | ets/partUpdSlider.ets | :no_entry: | Callback prints not found ||
| Stepper | ets/partUpdStepperIf.ets | :no_entry: | Callback prints not found ||
| Tabs | ets/partUpdTabsStatic.ets | :no_entry: | Callback prints not found ||
| TextInput | ets/partUpdTextInput.ets | :no_entry: | Callback prints not found ||
| Toggle | ets/partUpdToggle.ets | :no_entry: | Callback prints not found ||


## Universal events and attributes
| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| Click, Touch, Show/hide, Drag/drop, Mouse events| ets/partUpdUniversalEvents.ets | BL :interrobang: </br> :no_entry: | **Problem in baseline:**  Drag and drop doesn't render the dragged component but events come ok. Mouse events don't work at all. </br> **In multithread:**  Only show/hide works | Not creating issues now, not sure if they are supposed to work yet |
| Size, LayoutWeight, Direction, Anchor, Offset | ets/partUpdUniversalSizePos.ets | :white_check_mark: | |


# References

Page load and user interaction code workflow in multithreaded solution described at https://gitee.com/guido_grassel/ace_reports/blob/master/multithreaded-diagram.md

## Source code branches
### Windows Preview

Baseline branch: https://gitee.com/tsvetanstefanovski/ace_ace_engine/tree/baseline_april21/
Multithread development branch: https://gitee.com/tsvetanstefanovski/ace_ace_engine/tree/multithreaded_april21/
This branch is rebased on top of baseline_april21

### AOSP

Baseline branch: https://gitee.com/tsvetanstefanovski/android/tree/master_april21/
Multithreaded development WIP branch: https://gitee.com/tsvetanstefanovski/android/tree/multithreaded_april21/


