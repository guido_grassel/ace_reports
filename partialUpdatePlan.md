# Partial Update Solution Plan

> References to development branches and design doc can be found at the bottom of this page, updated June 15.

- [Partial Update Solution Plan](#partial-update-solution-plan)
- [Plan Summary](#plan-summary)
- [Partial Update Benefit](#partial-update-benefit)
  - [Covid19 benchmark app](#covid19-benchmark-app)
  - [guessTheImage benchmarlk app](#guesstheimage-benchmarlk-app)
- [Overview of Partial Update Functional Testing](#overview-of-partial-update-functional-testing)
- [Partial Update Component Testing Status](#partial-update-component-testing-status)
  - [Basic Components](#basic-components)
  - [Custom Components](#custom-components)
  - [Row, Column, Stack, Flex Containers](#row-column-stack-flex-containers)
  - [Specialized Containers](#specialized-containers)
  - [List](#list)
  - [Grid](#grid)
  - [Swiper](#swiper)
  - [Drawing Components](#drawing-components)
  - [Canvas Components](#canvas-components)
  - [Universal events and attributes](#universal-events-and-attributes)
- [Use Testing Team's apps for Further Functional Testing](#use-testing-teams-apps-for-further-functional-testing)
- [References](#references)
- [Development Resources](#development-resources)

# Plan Summary

1. Solution development and testing phase - current phase
    * functional testing and benefit verification
    * the progress is far enough that we cam start phase 2 while still finishing phase 1
2. HQ transpiler development phase 
    * HQ transpiler development, by Lihong's team. Expected result: Command line ace-ets2bundle with partial update support in a feature branch, 
    * Verify with Finland test cases the HQ transpiler produces correct output.
    * Test team's 4 applications ('fangtaobao', 'fangtoutiao', 'fangweixinETS', 'videoshort' available from testing team's GIT servers in YZ). 
    * Finland team role: info and support to transpiler team, framework bug fixing 
    * HQ and Finland team work together to get testing team's app to work (>=2 full time persons from HQ needed)
3. Independent app migration phase 
    * Migrate OHOS system apps to partial update, needs app developer involvement. Apps we find in the source tree on gitee are the following.
Apps we have knowledge about
        * Photos: applications/standard/photos
        * Settings: applications/standard/settings
        * Camera: application/standard/camera
        * systemui: application/standard/systemui
        * permission manager: base/security/access_token/frameworks/com.ohos.permissionmanager/permissionmanager
        * ***Question***: Where is launcher?
    * HQ team works with app developers to get their apps working (>=2 full time persons from HQ needed)
    * Finland team role: partial update framework bug fixing
    * Questions: 
        * Are OHO Photos and OHOS Settings the relevant apps, or are there others?
        * The IDE team should make their own plan, what support the app developers need from them.
4. Merge partial update to master

Questions:
1. When can Lihong's team start with phase 2 work ?
2. HQ needs to confirm to provide >=2 full time persons for apps migration in phases 2 and 3.
2. What are the apps developed by others (not framework and not testing team) ? Can we get access?
3. We are making the assumption here that partial update lands before multithread. Need to check the situation again as multithread progresses. Do you agree with this planning assumption.


# Partial Update Benefit

Goal: Verify the benefit of partial update by comparing its performance with baseline with real world benchmark apps.

## Covid19 benchmark app

Covid19 benchmark app tests both page load and several UI update scenarios.
Shows 20+ % performance increase in applicable UI update scenarios, no performance regression for page load and scenarios where the entire UI updates

## guessTheImage benchmarlk app

guessTheImage_2 has been added more recently. It is useful for benchmarking page load and UI update scenarios. 
It stresses on two aspects: 
* Repeated GridItems: ForEach with a fairly large data array (array length 400) 
* 400 (small) image loading.

So far we have measure UI update scenario. Shows 45-50% performance increase in a scenario where 100 out of 400 Image components within a Grid component are moved (no additions or deletions).

# Overview of Partial Update Functional Testing

Goal: Ensure partial update works as expected.

How ?
1. UI Component Testing - on-goung,  see following chapter.
2. Test with apps HQ has made - to be done, see following chapter

# Partial Update Component Testing Status

Goal: Ensure partial update works as expected.

* Test both initial render, but especially UI update scenarios.
* Cover all of framework's _documented_ components.
* Make a educated and reasonable choice of combinatory tests: 
    - UI component + attribute functiona, 
    - UI component + If/ForEach/LazyForEach

Caution:
- We are following the UI component docuemntation at [gitee.com](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/Readme-EN.md). We ignore any other components that the framework might have support for until we receive documentation for them.
- Referenced docuemntation of UI components and their attribute functions lacks details, examples are pretty basic. We usually based our own tests on the examples and added UI updates to them. We compare to baseline. If it works the same we decide the test is OK. We inevitably miss testing some functionality that we do not know exists.
- We encountered several cases where UI updates do not work on baseline.  We filed issues on gitee. Documented below. HQ needs to either fix baseline, or define that such attribute is set-once and can not be updated dynamically. Only then we can proceed.



| Icons| Status |
|---|---|
| :white_check_mark: | partial update is OK | 
| :no_entry: | partial update still has issues. See comment what the issue is | 
| BL :interrobang: | baseline has issue, and/or insufficient/unclear documentation. We can not test on partial update. Follow link to issue on gitee.com |


## Basic Components

| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| [Blank](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-blank.md) | ets/partUpdBlank.ets | :white_check_mark: | | |
| [Button](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-button.md) | ets/partUpdButton.ets </br> ets/partUpdButtonLabel </br> ets/partUpdButtonTextChild.ets | :white_check_mark: | | |
| [DataPanel](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-datapanel.md) | ets/partUpdDataPanel.ets | :white_check_mark: || |
| [DatePicker](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-datepicker.md) | ets/partUpdDatePicker.ets | :no_entry: |  **Problem in PU:**  setting 'selected' date and lunar mode fails | [DatePicker doesn't select given date](https://gitee.com/openharmony/ace_ace_engine/issues/I552HV)
| [Divider](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-divider.md) | ets/partUpdDivider.ets | :white_check_mark: || |
| [Gauge](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-gauge.md) | ets/partUpdGauge.ets | :white_check_mark: || |
| [Image](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-image.md) | ets/partUpdTabsBuilder.ets | :white_check_mark: | tests update of src attribute ok | |
| [ImageAnimator](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-imageanimator.md) | ets/partUpdImageAnimator.ets | BL :interrobang: | **Problem in baseline:**  ImageAnimator works a bit unreliably at best, switching states, direction and amount of iterations can get stop or show images in wrong order, but it continues eventually when clicking the buttons enough. Loads the second image on startup. </br>  **On PU**  it loads the second image and gets totally unresponsive by clicking once any button, I had to kill it from Task manager every time. Logs show that both JS and UI threads are stuck. | [ImageAnimator is not reliable](https://gitee.com/openharmony/ace_ace_engine/issues/I4ZZY3?from=project-issue) |
| [LoadingProgress](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-loadingprogress.md) | ets/partUpdLoadingProgress.ets | :white_check_mark: | | |
| [Marque](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-marquee.md) | ets/partUpdMarquee.ets  | :white_check_mark: | | |
| [Progress](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-progress.md) | ets/partupdProgress.ets | :white_check_mark: | Changes to value, color, width **works**, changes to parent component attributes **works** | |
| [QRCode](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-qrcode.md) | ets/partUpdQRCode.ets | :white_check_mark: || |
| [Radio](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-radio.md) | ets/partUpdRadio.ets | :white_check_mark: ||
| [Rating](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-rating.md) | ets/partUpdRating.ets | :white_check_mark: || |
| [Search](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-search.md) | ets/partUpdSearch.ets | :no_entry: | **Problem in PU:**  App crashes when trying to toggle placeholder or input search string | [Search component loses input](https://gitee.com/openharmony/ace_ace_engine/issues/I552II) |
| [Select](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-select.md) | ets/partUpdSelect.ets | :no_entry: | **Problem in PU:**  Doesn't select the next item and crashes when selectig item by mouse | |
| [Span](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-span.md) | ets/spanBuiltin.ets | :white_check_mark:  | [Guido], check and file issue about documentation| |
| [Slider](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-slider.md) | ets/partupdSlider.ets | :white_check_mark: || |
| [Stepper](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-stepper.md) </br> [StepperItem](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-stepperitem.md) | ets/partUpdStepperIf.ets | BL :interrobang: |  **Problem in baseline:**  Can not add StepperItems conditionally. </br>  **In PU**  there's deviation to baseline: after stepping to "Page Third", toggle page 5. Baseline shows "There's one more" on the right button, PU still shows "Finish". | [Conditional Stepper item](https://gitee.com/openharmony/ace_ace_engine/issues/I4XT42) |
| [Text](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-text.md) | many test cases that change label exist </br> ets/partUpdText.ets - tests everything on Text | :white_check_mark: || |
| [TextArea](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-textarea.md) | ets/partUpdTextArea.ets | :white_check_mark: || |
| [TextInput](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-textinput.md) | ets/partUpTextInput.ets | :white_check_mark: | tests value and placeholder update ok | |
| [Toggle](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-toggle.md) | ets/partUpdToggle.ets | BL :interrobang: | **Problem in baseline:**  Selecting the top-left toggle doesn't stick, it goes back to left by itself. </br> **Problem in PU:**  Toggling crashes the app| [Toggle value update](https://gitee.com/openharmony/ace_ace_engine/issues/I4W895)|
| [Video](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-media-components-video.md) | ets/partUpdVideo.ets | :white_check_mark: |  _**Video component not supported on windows**_ , but can show preview images: press stop, then src and then previewUri in sequence, it will alter between the 2 images. Works the same way on both branches. Should be properly tested on device. | |


## Custom Components

@Component, @State, @Link, @Prop :white_check_mark: 

| Test case | What tested | Notes | 
| --------- | ----------- | ----- |
| ets/partUpdBuiltinChildren.ets |  Single @Component |
| ets/partUpdCustomChildren.ets </br> ets/partUpdNestedCustomChildren.ets - children and grand children </br> ets/partUpdLinkProp.ets tests @State @Link, @Prop </br> ets/partUpdMixedChildrenSimpleType.ets </br> ets/partUpdMixedChildrenObjectType.ets tests @State @Link | Multiple @Component, state vars |
| ets/covid19.ets | benchmark app with many @Component use @State, @Link, @Prop, @Objectlink | |
| ets/guessTheImage_1.ets <br> ets/guessTheImage_2.ets | two versions of benchmark app with multiple @Component | |

## Row, Column, Stack, Flex Containers

Core containers components require testing in combination with If and ForEach, and  updating attribute functions on Containers.

Status summary:
* [Row](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-row.md) :white_check_mark:
* [Column](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-column.md) :white_check_mark:
* [Stack](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-stack.md) :white_check_mark:
* [Flex](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-stack.md) :white_check_mark:

TODO: We should check the attribute functions and make some more tests updating those.

| Test case | What tested | Status | Notes |
| --------- | ----------- | :----: | ----- | 
| ets/partUpdIf.ets - one branch </br> ets/partUpdIfElse.ets - if else branches </br> ets/partUpdIfElseIfElse.ets  - more branches | IF inside Column | :white_check_mark: ||
| ets/partUpdIfCustomComponent1.ets </br> ets/partUpdIfCustomComponent2.ets </br> ets/ partUpdIfCustomComponentNested.ets | Conditional Child: @Component inside If inside Column | :white_check_mark: | |
| ets/partUpdRowIfElse.ets | IF inside Row | :white_check_mark: | |
| ets/partUpStackIf.ets | IF inside Stack | :white_check_mark: | |
| ets/partUpdForEachSimple.ets | ForEach inside Column | :white_check_mark: | |
| ets/partUpdForEachCustomComp.ets </br> ets/partUpdForEachObjectLink.ets - with @ObjectLink | repeated @Component: Child @Component inside ForEach inside Column | :white_check_mark: | |
| ets/partUpdForEachIf.ets | Container > ForEach > IF | :no_entry: | FIXME Check the status ! |
| ets/partUpdFlexIf.ets | conditional child inside Flex container: Flex > IF > Text | :white_check_mark: |  |


## Specialized Containers

Status summary: 
* There is still some fixing to be done. The fixes we have done so far, were usually quite small. Understanding the component implementation is what takes most of our time. Only after we understand how it works we can find the solution.
* Several  components have issues with UI updates already on baseline. The documentation does not tell that such updates would not be supported. We do not work on partial update support until the situation is clear, which is either: a) fix the component on baseline or b) update the documentation that such attribute is set-once, it can not be updated while the component is visible.


| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| [AlphabetIndexer](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-alphabet-indexer.md) | ets/partUpdAlphabetIndexerIf.ets | :no_entry: |  **Problem in PU:**  Adding an item crashes the app |[AlphabetIndexer does not update when adding items](https://gitee.com/openharmony/ace_ace_engine/issues/I4XZ9F)| 
| [Badge](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-badge.md) | ets/partUpdBadgeIf.ets | :white_check_mark: | Fix in RenderBadge exist, [ Guido file a documentation issue ]||
| [ColumnSplit](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-columnsplit.md) | ets/partUpdColumnSplitIf.ets </br> ets/partUpdForEachColumnSplit.ets | :white_check_mark: | ||
| [Counter](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-counter.md) | ets/partUpdCounterIf.ets | :no_entry: | NOK, toggling color and height don't do anything ||
| [GridContainer](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-gridcontainer.md) | ets/partUpdGridContainerIfSimple.ets | :no_entry: | *FAILS* it appears GridContainer is not a Component of its own but modifies or wraps Components; Investigating ||
| [Navigator](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-navigator.md) | ets/partUpdNavigator.ets </br> ets/partUpdNavigatorDetail.ets </br> ets/partUpdNavigatorBack.ets | BL :interrobang: | **Problem in baseline:**  setting the Navigator active true or false doesn't have effect to the component, it navigates to the page even when not active. It goes index -> Detail -> Back -> index, but works only once, starting a new round shows an empty page. | [Navigator can not be set inactive](https://gitee.com/openharmony/ace_ace_engine/issues/I526TQ?from=project-issue)|
| [Navigation](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-navigation.md) | | | todo ||
| [Panel](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-panel.md) | ets/partUpdPanelIf.ets | :white_check_mark: |||
| [Refresh](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-refresh.md) | ets/partUpdRefreshIf.ets | BL :interrobang: | **Problem in baseline:**  The component seems to want to control it's refreshing status itself even tough the `@State isRefreshing` variable is given to it from outside. The component does not update the refreshing state correctly, it never stops showing the spinner. Also modifying the `offset` and `friction` parameters from the initial values makes the usage of the component only worse. The documentation does not mention neither of the parameters affecting the size of the spinner, but they do and it's not systematic. They don't seem to be doing what they are described to do. | Not creating any issues right now, maybe it's better when we rebase... |
| [RowSplit](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-rowsplit.md) | ets/partUpdRowSplitIf.ets </br> ets/partUpdForEachRowSplit.ets | :white_check_mark: ||
| [Scroll](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-scroll.md) | ets/partUpdScrollForEach.ets |  :white_check_mark: ||
| [ScrollBar](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-basic-components-scrollbar.md) | ets/partUpdScrollBar.ets | BL :interrobang: |  **Problem in baseline:**  It is not possible to turn the scrollbar into horizontal mode, even though the Scroll and Scroller turn. </br>  **In PU:**  toggling the scrollbar visibility does not update correctly when turning it to Off, the background stays visible. | [ScrollBar can't be used in horizontal mode](https://gitee.com/openharmony/ace_ace_engine/issues/I54V77) |
| [SideBarContainer](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-sidebarcontainer.md) | ets/partUpdSideBarContainer.ets | BL :interrobang: </br> :no_entry: |  Width changes work in BL and PU after fixing layout issue. Issue in PU: after one re-render menu button no longer works. Toggling menu button visibility does not work. | PU fix TODO |
| [Tabs](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-tabs.md) </br> [TabContent](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-tabcontent.md) | ets/partUpdTabsStatic.ets </br> ets/partUpdTabsBuilder.ets | :no_entry: | Switching tabs works, segmentation fail on updating tab label ***FAILS*** </br> TabContent.tabBar(builder: @Builder function) re-renders Text and Image within the @Builder function on every tab ndex change - it works! ||

## List

[List](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-list.md) container status summary: :white_check_mark: - some more advanced test cases, as mentioned below, should be developed, though.


| Test case | What tested | Status | Status Aug2 |
| --------- | ----------- | ---- | ---- |  
| ets/partUpdListStatic.ets | attribute changes only | ok | ok |
| partUpdListIfBranchId.ets <br> partUpdListIfElseNested.ets <br> partUpdListIfSimple.ets <br> partUpdListIfElseIf.ets | Conditional ListItems | ok | |
| partUpdListForEachCustComp.ets <br> partUpdListForEachObjectLink.ets  <br> partUpdListForEachSimple.ets  | repeated ListItems, using ForEach | OK | OK, JSView destructor is not called |
| ets/partUpdListLazyForEachSimple.ets| repeated ListItems, using LazyForEach | ok | OK  | 
| ets/partUpdListLazyForEachCustComponent.ets | repeated ListItems, using LazyForEach, with Cust Component inside | ok | NOK, JSView destructor not called  | 
| ets/partUpdListNonLazy.ets | repeated ListItems with ForEach, Items are created with eagr (non-lazy) code path) | OK | works, but is it reated non-lazy? |
| partUpdListIfLazyForEach.ets | to be clarified and fixed test case | TODO |  |
| TODO | conditional repated ListItems: ForEach and LazyforEach, in combination with If | - | |
| TODO | two forEach inside same List | - | |



## Grid

[Grid](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-grid.md) container - :white_check_mark: Works except an obscure test case which combines LazyForEach and If, not even sure this is meant to be supported.
 

| Test case | What tested | Status | Aug2 | 
| --------- | ----------- | ---- | ---- |  
| ets/partUpdGridForEachNonLazy.ets | Grid > ForEach > GridItem that either creates in lazy or in eager way, both tested | OK | | 
| ets/partUpdGridNestedForEach.ets | GridItems created by two nested ForEach loops | OK | |
| ets/partUpdGridForEachCustComp.ets | repated GridItems (by ForEach) that have a custom container as child | OK | NOK, JSView destructor not called |
| ets/partUpdGridForEachIf.ets | Repeated, conditional GridItem (with ForEach) | ok | ok |
| ets/partUpdGridLazyForEach.ets | Repeated GridItems, with LazyForEach | ok |
| ets/partUpdGridLazyForEachIf.ets | Repeated conditional GridItems, with LazyForEach and If | FAILS this one is obscure, todo, check if it works on baseline! | NOK segmentation error |
| partUpdGridForEachModAttrSimpleProxied | mod number of rows | | OK |
| partUpdGridForEachModAttrNonProxied | non proxied code path | | OK |



## Swiper 

[Swiper](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-container-swiper.md) container status summary: :no_entry: 

Swiper with static list of children, with conditional (If) and repeated children using ForEach is mostly working. Two issues left: 
1. Repeated children with LazyForEach is expected to take some time. 
2. UI update issue in a specific scenario: Delete a child that is before the currently viewed child.  This change does not update the 'dots' position incator and also Swiper scrolling mechanism seems not to realize. 

| Test case | What tested | Status | Aug 2
| --------- | ----------- | :----: |  :----: |  
| ets/swiperBuiltinStatic.ets | Swiper with static list of children, update Swiper child attr | OK  | |
| ets/swiperBuiltinIf.ets <br> ets/swiperBuiltinIfElse.ets | Conditional Swiper child | on-going: removing child that is to the left of current viewed child has sometimes issue . Dot position indication does not update. Scroll mechanism seems not to realize the deletion.| dito |
| ets/swiperBuiltinForEach.ets | Repeated Swiper children with ForEach | on-going: same issue as If case | OK |
| swiperBuiltinLazyForEach.ets | Repeated Swiper children with LazyforEach | FAILS, Swiper with LazyforEach to be done | OK |


## Drawing Components

| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| [Circle](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-circle.md) | ets/partUpdCircle.ets  | :white_check_mark: ||
| [Rect](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-rect.m ) | ets/partUpdRect.ets | :white_check_mark: ||
| [Ellipse](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-ellipse.md) | ets/partUpdEllipseIf.ets | :white_check_mark: ||
| [Line](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-line.md) | ets/partUpdLineIf.ets | :white_check_mark: ||
| [Path](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-path.md) | ets/partUpdPath.ets | :white_check_mark: ||
| [Polyline](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-polyline.md) | ets/partUpdPolyline.ets | :white_check_mark: ||
| [Polygon](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-polygon.md) | ets/partUpdPolygon.ets | :white_check_mark: ||
| [Shape](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-drawing-components-shape.md) | ets/partUpdShape.ets | :white_check_mark: | OK-ish. Moving viewport works on both branches, but there's flickering and toggling antialias is worse on PU. |


## Canvas Components

Currently functions in the same way as the baseline Canvas. We proposed some API and implementation redesign in a separate document.

| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| [Canvas](https://gitee.com/openharmony/docs/blob/master/en/application-dev/reference/arkui-ts/ts-components-canvas-canvas.md) | ets/partUpdCanvas.ets </br> etc/partUpdCanvasRedrawSemantics.ets </br> ets/partUpdCanvasGradient.ets </br> ets/partUpdCanvasOffScreen.ets </br> ets/partUpdCanvasDrawPrimitive.ets| :white_check_mark: |  ||


## Universal events and attributes
| Component | Test case | Status | Notes | Issues |
| --------- | --------- | :----: | ----- | ------ |
| Click, Touch, Show/hide, Drag/drop, Mouse events| ets/partUpdUniversalEvents.ets | BL :interrobang: | **Problem in baseline:**  Drag and drop doesn't render the dragged component but events come ok. Mouse events don't work at all. | Not creating issues now, not sure if they are supposed to work yet |
| Size, LayoutWeight, Direction, Anchor, Offset | ets/partUpdUniversalSizePos.ets | :white_check_mark: | |


# Use Testing Team's apps for Further Functional Testing

4 Apps available from Testing Team (on their YZ GIT server, URL available Huawei internally)
1. 'fangtaobao', 
2. 'fangtoutiao', 
3. 'fangweixinETS', 
4. 'videoshort'

Partial update requires extensive transpiler changes. We have made those changes to compiler-nt while developing partial update. 

Two phase approach:

1. phase - use to-be extended compiler-nt, attempt to get parts of these apps compile with compiler-nt so that they can be tested with partial update before the HQ transpiler has partial update support.
2. phase - use HQ transpiler once it has partial; update support.

The main needed extension to compiler-nt is to support multiple ETS input files / imports. We will check if we can do these.
Then various types and attribute functions are misisng from its TypeScript declaration file. These can be added.



# References

Partial Update solution design (sorry Huawei internal link): https://rnd-gitlab-eu-c.huawei.com/zidanehbs/ace-fin-web/ace-reports/-/blob/master/new_feature_ideation/partial-update-optimisation-for-ohos.md


# Development Resources

Source code, currently June14 baseline:
* Partial update: https://gitee.com/arkui-finland/arkui_ace_engine/tree/baseline_june14/
* Baseline: https://gitee.com/arkui-finland/arkui_ace_engine/tree/partial-update-june14/

Compiler NT:
* compile for baseline code: https://gitee.com/arkui-finland/ace-compiler-nt/tree/master/
* compile for partial update: https://gitee.com/arkui-finland/ace-compiler-nt/tree/partial_update/
* clone to project root directory

App Samples
* modifications to compile with Compiler NT, for baseline and for partial update: https://gitee.com/arkui-finland/app_samples/tree/compiler-nt/
* clone to project root directory

Download ready archive of Partial update, Baseline, prebuilds:
`arkui.life:/home/archive/baseline+partial-update+OpenHarmony-prebuilds--june14.tar.gz`


How to build baseline and partial update (in this order):

1. `cd ./foundation/arkui/ace_engine/frameworks/bridge/declarative_frontend/state_mgmt`, `npm install`, `npm run build` or `npm run build_release` - this step generates `state_mgmt/dist/stateMgmt.js` and deletes `js_proxy_class.cpp` from `./out/....`. Next native compilation will generate a new `js_proxy_class.cpp` from `state_mgmt/dist/stateMgmt.js`.
2. `./build.sh ...`
3. `cd ./ace-compiler-nt` and `node compile ets/xxxx.ets`
4. transfer DLLs/EXEs and compiled app JS to Windows and run.


### AOSP

* arkui_ace_engine: `git@gitee.com:arkui-finland/arkui_ace_engine.git` branches `aosp_baseline_june14` and `aosp_partial_update_june14`.
* ArkUI Android Adapter aka 'android' as called in the arkui-cross repo, mounts to arkui/ace_engine/adapter: https://gitee.com/arkui-finland/android-adapter/tree/master/  branches `aosp_baseline_june14` and `aosp_partial_update_june14` (as of June16 the only changes are to enable systrace)
* Android NDK project:  git@gitee.com:sunfei2021/ace-test-source.git
* Compiler-NT: see Windows Preview
* Archive on arkui.life: `home/archive/aosp-baseline+partial-update+prebuilds-june14.tar.gz`
