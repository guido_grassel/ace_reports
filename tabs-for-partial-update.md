# Tabs changes for partial update

## Issues with current implementation:

1. The main problem is that part of the component and element trees that defines tab bar is not exposed in ets/js files. That tab bar subtree created and maintained internally by the framework. That means that regular partial update code that collects and maintains dependency of UI components and state variables fails to track dependency of tab bar items and state variables.  
Tab bar item component created by js_tab_content on update of @state variable. In a sense that is a sibling branch in the tree and that branch depends on the same variable that TabContent depends on.  
For the partial update rerender case we do not construct the whole component tree for the view and for tabs and because of that we can not find neither TabsComponent nor TabBarComponent that acts as a parent for Tab Bar Item. We have to invent a new mechanism to create/update corresponding tab bar item when we update for tab content.

2. For the not yet shown tab we have only the component tree created on the initial render. The element and render node trees are not created before it is actually shown. At the same time partial update in re-render step updates existing elements, not components. That means that in the described case of not shown tab content we do not have elements and partial updates failed to find the correct elements. When such a tab actually goes into view it will reflect the state application had on the initial render, since the component tree was created at that time.  
After the tab is shown we have the element tree created and subsequent updates work as designed.

3. In case the tab content pane is shown conditionally (it is wrapped in IF component). Baseline implementation of V2::TabContentProxyElement is not prepared to handle on the fly changes in the number of tabs.



## High level changes:

* Component tree for tab content is not created for tabs that were not shown at least once (baseline creates component tree for those tabs).  
TabContentItemComponent now has a builder_ function that executed only when tab gets actually visible. That builder_ function executed by TabContentItemElement::PerformBuild() method. After that we have both component and element trees in place.

* JSTabContent::Create, JSTabContent::SetTabBar do not create items for the tab bar in partial update mode at all.

* Items in the tab bar are created and removed by TabContentItemElementProxy. TabContentItemElementProxy created initially for all all (including not yet shown TabContentItemElements). There are no TabContentItemElement instances created for tabs that have not been shown at least once.

* TabContentItemElement points to corresponding TabBarItemElement with the help of  tabBarItemElementId_class variable.

* Because partial update works with elements there is a a lot of code for traversal of tabs Element tree 

## Tree structure
```
<TabsElement>
    <BoxElement>
        <TabBar>
            <TabBarIndicator>
            </TabBarIndicator>
            <TabBarITem/>
            ...

        </TabBar>
    </BoxElement>
  
    <FlexItemElement>
        <TabContentProxyElement>
            <BoxElement>
                <TabContentItemElement/>
            </BoxElement>
        </TabContentProxyElement>
     </FlexItemElement>
     ...

<TabsElement>
```
